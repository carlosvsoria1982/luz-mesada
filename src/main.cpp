//Modificiones para probar el nuevo branch
//TEsting
//utilizao ammend

#include <Arduino.h>
#include <WiFiManager.h>
#include <NtpClient.h>
#include <PubSubClient.h>
#include <Ticker.h>

int led_pin=2;
int salida=13;

int led_status=0; //variable que indica si el estado actual del led; encendido->1 o apagado->0
int led_status_old=1;
int envio=0;

////////////Variables utilizadas para obtener el tiempo///////////

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "0.south-america.pool.ntp.org", 3600, 60000);
////////////// variables utilizadas para realizar el time stamp//////////////
String formattedDate;
String dayStamp;
String timeStamp;

//Variables utilizadas para la conexión WiFi

const char* ssid     = "Quincho";
const char* password = "17041010";
char* status_wifi ="";
char* ip_address="";
char* mac_adress="";

byte mac[6];

IPAddress Ip;
long rssi = 0;

//////////////////MQTT//////////////////

const char* mqtt_server = "186.159.120.14"; //Server utilizado en Chirpstack de Xfly
WiFiClient espClient;
PubSubClient client(espClient);

long lastMsg = 0;
char msg[50];
int value = 0;

int mqtt_action = 0;
int mqtt_new=0;
int mqtt_status=0;

//////////////////Sensor de movimiento ////////////////
int movimiento=34;
#define timeSeconds 10
// Set GPIOs for PIR Motion Sensor and push button

const int motionSensor = 14;  //GPIO14
const int pushButton = 12;  //GPIO12

int mov_status=0;
int boton_new=0;  //utilizada para indicar que hay un nuevo pulsado del boton
int boton_status=0; //utilizada para almacenar el estado del boton

// Timer: Auxiliary variables
unsigned long now = millis();
unsigned long lastTrigger = 0;
boolean startTimer = false;

////Timers
Ticker timer;
volatile int interrupts;

bool fin_interrupt = false;

void conexion()
{
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) { //Rutina para la espera de la conexión...
        delay(500);
        Serial.print(".");
    }
    timeClient.begin();
    timeClient.setTimeOffset(-10800);
    Serial.println("");
    Ip=WiFi.localIP();
    WiFi.macAddress(mac);   //obtenccion de dirección MAC

    /////// Aviso de Conexión /////
    Serial.println("WiFi connected");
    
    /////// Datos relevantes a la conexión //////
    Serial.println("IP address: ");
    Serial.println(Ip);
    //Serial.println(mac);
    ////// MAC  //////
    Serial.print("MAC: ");
    Serial.print(mac[0],HEX);
    Serial.print(":");
    Serial.print(mac[1],HEX);
    Serial.print(":");
    Serial.print(mac[2],HEX);
    Serial.print(":");
    Serial.print(mac[3],HEX);
    Serial.print(":");
    Serial.print(mac[4],HEX);
    Serial.print(":");
    Serial.println(mac[5],HEX);
    ////// RSSI  //////
    Serial.print("RSSI: ");
    rssi=WiFi.RSSI();
    Serial.print(rssi);
    Serial.println("");
    
}

void callback(char* topic, byte* payload, unsigned int length) {  //Función utilizada para la recepción de los mensajes que son publicados en el tópico de interés
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    mqtt_action = 1;
    
    
    //digitalWrite(led_pin, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {

    mqtt_action=0;
    
    //digitalWrite(led_pin, HIGH);  // Turn the LED off by making the voltage HIGH
  }
mqtt_new=1;
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// Checks if motion was detected, sets LED HIGH and starts a timer
ICACHE_RAM_ATTR void detectsMovement() {
  Serial.println("MOTION DETECTED!!!");
  //digitalWrite(led_pin, LOW);
  startTimer = true;
  mov_status=1;
  lastTrigger = millis();
}

ICACHE_RAM_ATTR void detectsPush() {

  Serial.println("PUSH DETECTED!!!");
  
  //delay(50);
  timer1_write(2500000);
}

void ICACHE_RAM_ATTR onTime() {
	interrupts++;
	Serial.print("Total Ticks:");
	Serial.println(interrupts);
  boton_new=1;
	// Re-Arm the timer as using TIM_SINGLE
	//timer1_write(312500);//12us
}

void setup() {
  // put your setup code here, to run once:
pinMode(salida,OUTPUT);
pinMode(led_pin,OUTPUT);
Serial.begin(115200);
delay(10);
conexion();
client.setServer(mqtt_server, 1883);
client.setCallback(callback);

// PIR Motion Sensor mode INPUT_PULLUP
pinMode(motionSensor, INPUT_PULLUP);
// Set motionSensor pin as interrupt, assign interrupt function and set RISING mode
attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, RISING);


// PIR Motion Sensor mode INPUT_PULLUP
pinMode(pushButton, INPUT_PULLUP);
// Set motionSensor pin as interrupt, assign interrupt function and set RISING mode
attachInterrupt(digitalPinToInterrupt(pushButton), detectsPush, FALLING);

//////////////configuración de Timer//////////////
timer1_attachInterrupt(onTime); // Add ISR Function
timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
// Arm the Timer for our 0.5s Interval
timer1_write(2500000); // 2500000 / 5 ticks per us from TIM_DIV16 == 500,000 us interval 

////Testeo de Inicio////
digitalWrite(salida,1);
delay(4000);
//digitalWrite(salida,0);
//delay(4000);

//////////////////////////

}

void loop() {

 

   

  /////////////Se realiza la primer conexión al router WiFi/////////////////
  
  /////////////Conversion de MAC/////////////////
  //array_to_string(mac, 6, mac_char);
  //Serial.println(mac_char);
  //////////////////////////////////////////////
  
   if (!client.connected()) {
    reconnect();
  }
  client.loop();

  /*
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    snprintf (msg, 50, "hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("outTopic", msg);
  }
  // put your main code here, to run repeatedly:
*/
  ////////////////// Código para apagar el led luego de la detección de movimiento
  now = millis();
  // Turn off the LED after the number of seconds defined in the timeSeconds variable
  if(startTimer && (now - lastTrigger > (timeSeconds*1000)))
  {
    Serial.println("Motion stopped...");
    //driver_led_new = 1;
    //digitalWrite(led_pin, HIGH);   //Encender led
    //led_status=0;
    mov_status=0;
    startTimer = false;
  //////////////////
  }

  
  
if  (boton_new==1)    //existió un nuevo pulsado del boton
    {
      boton_new=0;
      if (boton_status==1)  //Si el estado del boton es Activo...Desactivarlo
       {
         Serial.println("Status Boton: Desactivo...");
         boton_status=0;
       }
      else  //Si el estado del boton es Desactivo...Activarlo
       {                   
         Serial.println("Status Boton: Activo...");
         boton_status=1;
       }

    }
 
  

if (mqtt_new==1)
  {
    mqtt_new=0; //ya se refrezcaron los valores desde mqtt; se espera una nueva actualización
    if (mqtt_action==1)
      {
        Serial.println("encendido por MQTT...");
        //digitalWrite(led_pin, LOW);   //Encender led
        boton_status=1;
      }
    else
    {
      Serial.println("apagado por MQTT...");
      //digitalWrite(led_pin, HIGH);   //Apagar led
      boton_status=0;
    }
    
  }
  


if (led_status_old==0)      //El led se encuentra apagado
  {
    if ((mov_status==1) || (boton_status==1))
    { 
      //Serial.println("Movimiento o boton activado-> Encender Led");
      
      if (led_status==0)
      {
      snprintf (msg, 50, "Luz encendida");
      Serial.print("Publish message: ");
      Serial.println(msg);
      client.publish("outTopic", msg);
      led_status=1;
      }
      led_status_old=led_status;
      digitalWrite(led_pin, LOW);   //apagar led
      digitalWrite(salida, LOW);   //apagar led
    }
  }
else    //led_status_old==1
{                       //El led se encuentra encendido
  if (((mov_status==0) && (boton_status==0)))
    { 
      //Serial.println("Movimiento finalizado o boton desactivado-> Apagar Led");
      
      if (led_status==1)
      {   
      snprintf (msg, 50, "Luz Apagada");
      Serial.print("Publish message: ");
      Serial.println(msg);
      client.publish("outTopic", msg);
      led_status=0;
      }
      led_status_old=led_status;
      digitalWrite(led_pin, HIGH);   //Apagar led
      digitalWrite(salida, HIGH);   //Apagar led 
    }
}
          //El led se encuentra encendido
}